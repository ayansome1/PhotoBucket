package com.example.this_is_ayan.photobucket;

/**
 * Created by this_is_ayan on 31-03-2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;


public class DBHandler extends SQLiteOpenHelper
{
    String stringSearchedGlobal;
    int pageNumberGlobal;

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "ImageDatabase.db";
    private static final String TABLE_NAME = "image_table";
    private static final String KEY_ID = "_id";
    private static final String SEARCH_STRING = "searchString";
    private static final String IMAGE_URL = "imageURL";
    private static final String PAGE_NUMBER = "pageNumber";

    String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ("+KEY_ID+" TEXT PRIMARY KEY,"+ SEARCH_STRING +" TEXT,"+ IMAGE_URL +" TEXT,"+ PAGE_NUMBER +" INTEGER)";
   //String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ("+IMAGE_URL +" TEXT PRIMARY KEY,"+ SEARCH_STRING +" TEXT,"+  PAGE_NUMBER +" INTEGER)";

    String DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE_NAME;

    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    //@Override
    public void addImage(GridItem griditem,String stringSearched,int pageNumber)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            String k=String.valueOf(griditem.getId());
            values.put(KEY_ID, k);
            System.out.println("**here image id is**" + griditem.getId());
            values.put(IMAGE_URL, griditem.getImage());
            System.out.println("**here image url is**" + griditem.getImage());
            values.put(SEARCH_STRING, griditem.getSearchString());
            System.out.println("**here search string is**" + griditem.getSearchString());
            values.put(PAGE_NUMBER, griditem.getPageNumber());
            System.out.println("**here page number is**" + griditem.getPageNumber());

            db.insertWithOnConflict(TABLE_NAME, null, values,SQLiteDatabase.CONFLICT_IGNORE);
            db.close();
        }catch (Exception e){
            Log.e("problem",e+"");
        }
    }

   // @Override
    public ArrayList<GridItem> getAllImages(String stringSearched,int pageNumber)
    {
        stringSearchedGlobal=stringSearched;
        pageNumberGlobal=pageNumber;

        SQLiteDatabase db = this.getReadableDatabase();
       // ArrayList<GridItem> imageList = null;
        ArrayList<GridItem> imageList = new ArrayList<GridItem>();

        try{
          //  imageList = new ArrayList<GridItem>();
            String QUERY = "SELECT * FROM "+TABLE_NAME+" WHERE "+SEARCH_STRING+"="+"'"+stringSearched+"'"+" AND "+PAGE_NUMBER+"="+pageNumber;
            System.out.println("****"+QUERY);

            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    GridItem image = new GridItem();


                    image.setId(cursor.getInt(0));
                    System.out.println("**image id**" + cursor.getInt(0));

                    image.setSearchString(cursor.getString(1));
                    System.out.println("**search string**" + cursor.getString(1));

                    image.setImage(cursor.getString(2));
                    System.out.println("**URL**" + cursor.getString(2));

                    image.setPageNumber(cursor.getInt(3));
                    System.out.println("**page number**" + cursor.getInt(3));

                    imageList.add(image);

                }
            }
            db.close();
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return imageList;
    }

   // @Override
    public int getImageCount()
    {
        int num = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+TABLE_NAME+" WHERE "+SEARCH_STRING+"="+"'"+stringSearchedGlobal+"'"+" AND "+PAGE_NUMBER+"="+pageNumberGlobal;

            Cursor cursor = db.rawQuery(QUERY, null);
            num = cursor.getCount();
            db.close();
            return num;
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return 0;
    }
}