package com.example.this_is_ayan.photobucket;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class GridViewActivity extends ActionBarActivity
{
    //private static final String TAG = GridViewActivity.class.getSimpleName();
    GridView mGridView;
    int id;
    private ProgressBar mProgressBar;
    String KEY;
    InputMethodManager imm;
    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData,temp;
    String FEED_URL;
    EditText searchEditText;
    Button searchButton;
    String searchString;
    int i;
    boolean loadingMore;
    DBHandler handler;
    NetworkUtils utils;
    boolean flagFirstTime;
    int totalImages;
   // Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridview);
        flagFirstTime=true;
        totalImages=0;
        utils = new NetworkUtils(GridViewActivity.this);

        handler = new DBHandler(this);
        KEY = getResources().getString(R.string.KEY);
        searchEditText=(EditText)findViewById(R.id.searchEditText);
        searchButton=(Button)findViewById(R.id.searchButton);
        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
       // mContext=getApplicationContext();
        loadingMore=true;
        mGridData = new ArrayList<>();


        mGridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, mGridData);//////////


        mGridView.setAdapter(mGridAdapter);
        i=1;

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
                mGridData.clear();



                searchString = searchEditText.getText().toString();
                searchString.trim();
                searchString = searchString.replace(' ', '+');

                i=1;
                if(utils.isConnectingToInternet())
                {

                    FEED_URL = "http://pixabay.com/api/?key=" + KEY + "&q=" + searchString + "&image_type=photo&per_page=20&page="+i;
                    new AsyncHttpTask().execute(FEED_URL);
                    mProgressBar.setVisibility(View.VISIBLE);
                }
                else
                {
                    i=1;
                    loadingMore=true;
                    mGridData=handler.getAllImages(searchString,i);
                    System.out.println("count is " + handler.getImageCount());
                    totalImages=totalImages+handler.getImageCount();
                    mGridAdapter = new GridViewAdapter(GridViewActivity.this, R.layout.grid_item_layout, mGridData);//////////
                    mGridAdapter.setGridData(mGridData);
                    mGridView.setAdapter(mGridAdapter);
                    mGridAdapter.notifyDataSetChanged();
                    loadingMore=false;
                    mProgressBar.setVisibility(View.GONE);

                }








            }
        });






       // flagFirstTime=true;

        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView arg0, int arg1)
            {




            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount)
            {

                if (totalItemCount > 0) {
                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    System.out.println("first visible item " + firstVisibleItem);
                    System.out.println("visible item count" + visibleItemCount);
                    System.out.println("total item count " + totalItemCount);
                    System.out.println("last in screen " + lastInScreen);


                    if (lastInScreen == totalItemCount && loadingMore == false)
                    {

                        if (utils.isConnectingToInternet())
                        {
                            i++;
                            FEED_URL = "http://pixabay.com/api/?key=" + KEY + "&q=" + searchString + "&image_type=photo&per_page=20&page=" + i;
                            new AsyncHttpTask().execute(FEED_URL);
                            mProgressBar.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            System.out.println("entering here**********************************");
                            i++;
                            loadingMore = true;


                            System.out.println("##########image count is" + handler.getImageCount());
                           if(handler.getImageCount()>0)
                           {
                               temp = new ArrayList<GridItem>();
                               temp = handler.getAllImages(searchString, i);
                               mGridData.addAll(temp);
                               mGridAdapter.setGridData(mGridData);
                               mGridAdapter.notifyDataSetChanged();
                               loadingMore = false;
                               mProgressBar.setVisibility(View.GONE);
                           }



                        }
                      //  flagFirstTime=false;
                    }
                    // flagFirstTime=false;
                }
            }
        });








    }

    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params)
        {
            Integer result = 0;
            loadingMore=true;
            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Lets update UI

            if (result == 1)
            {
                mGridAdapter.setGridData(mGridData);
                loadingMore=false;
            }
            else
            {
                  Toast.makeText(GridViewActivity.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
        {
            result += line;
        }

        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     *
     * @param result
     */
    private void parseResult(String result)
    {
            try {
            JSONObject response = new JSONObject(result);
            JSONArray hits = response.optJSONArray("hits");
            GridItem item;
            int j=0;
            while(hits.optJSONObject(j)!=null)
            {
                JSONObject post = hits.optJSONObject(j);

                item = new GridItem();
                item.setImage(post.getString("previewURL"));

                id=post.getInt("id");
                item.setId(id);
                item.setSearchString(searchString);
                item.setPageNumber(i);
                mGridData.add(item);
                handler.addImage(item,searchString,i);



                j++;
            }
        } catch (JSONException e)
            {
            e.printStackTrace();
        }

    }
}